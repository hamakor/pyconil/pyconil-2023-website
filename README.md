Static version of PyCon Israel 2023 website
===========================================

We started with

```console
$ wget --mirror -E --show-progress https://2023.pycon.org.il/
```

This rewrites correctly all the internal links, but it leaves two
major problems. The first, and more major, is that we need the `2023/`
prefix in links. The second is that many links -- mostly used via
`src` or `link`, but probably some `href`s too -- point outside of the
website. At least those that go to https://uploads-ssl.webflow.com/
need to be downloaded into a "static" folder, and the links changed
accordingly.


So, the next step is
```console
$ for f in `find 2023.pycon.org.il/ -iname \*.html`; do 
    python3 -c '\
	    import sys, re; \
		print ("\n".join(re.findall("(https://.*?)\"", sys.stdin.read())))' \
	< $f >> urls.txt; 
  done
```

This collects all https urls in the html files into one list.
This list has 3088 lines, though -- many of them repeating and
others we're not interested in (true external links). We're only
interested in lines that have "webflow" in them:
```console
$ grep webflow urls.txt | sort | uniq > webflow-urls.txt
$ wc -l webflow-urls.txt 
122 webflow-urls.txt
```

Now, some of these lines still include several URLs in one line,
separated by commas; some of these urls are then annotated by a width
mark (some properties go `prop="url w500, url"`).

```py
In [1]: lines = open('webflow-urls.txt').readlines()

In [2]: unsplit = open('webflow-urls-1.txt', 'w')

In [3]: for line in lines:
   ...:     for part in line.split(','):
   ...:         part = part.split()[0]
   ...:         print(part.strip(), file=unsplit)
   ...: 
```

Luckily, we don't have any broken URLs in this output (there was no
legit comma in the middle of a URL). We sort and uniq again, and are
left with 168 URLs.

```
$ cat webflow-urls-1.txt | sort | uniq > webflow-urls-2.txt
```

Most of these urls are on the "uploads" server, which means
they're really ours and we want to keep serving them form our own
server.

We re-download, this time preparing properly for inclusion of these
statics:
```
$ mkdir 2023
$ cd 2023
$ grep uploads ../webflow-urls-2.txt | xargs \
    wget -nH -x --mirror -k -E https://2023.pycon.org.il/
```

The last command includes, in one download, both the site pages, and
the static pages. `-E` changes page names to end with proper suffixes
(`.html` etc). `-k` edits links inside the documents to make them
refer relatively to the downoaded files. `-x` tells wget to create
directories according to URLs; `-nH` says to ignore the host, so only
the path is used -- thus removing the difference between uploads.webflow
and our domain.

Gitlab Pages
------------

In order to deploy to Gitlab pages, some more changes are required.
First of all, the files must reside in a `public` folder, not in
`2023`.

Then, it turns out that gitlab pages, much like the file system,
does not automatically open `.css.gz` or `.js.gz`  files the way
wget expects. So we need to unzip those, and change the references to
them.

While at it -- the css file includes references to some more files
on the Webflow upload server. We'll fix these too.

First, unzip:

```console
$ cd public
$ gunzip 63cc2f4cd802df3c5b78883f/js/webflow.acaffe362.js.gz
$ gunzip 63cc2f4cd802df3c5b78883f/css/pycon-israel-2023.webflow.c47b99761.css.gz
```

Then, fetch the missing references. As common in CSS, the references
are enclosed in `url(...)`, and (by `grep`ping) we find them each on a
separate line. So we can select the lines, cut "after the first `(`" and
then "before the first `)`", and pass the results to `wget`:

```console
$ grep http 63cc2f4cd802df3c5b78883f/css/pycon-israel-2023.webflow.c47b99761.css \
     | cut -d\( -f2 \
     | cut -d\) -f1 \
     | xargs wget -x -nH 
```

Then make these URLs relative

```console
$ sed -i s=https://uploads-ssl.webflow.com/63cc2f4cd802df3c5b78883f/=../=g \
    63cc2f4cd802df3c5b78883f/css/pycon-israel-2023.webflow.c47b99761.css
```
and correct references to the JS and CSS

```console
$ find . -exec sed -i s/pycon-israel-2023.webflow.c47b99761.css.gz/pycon-israel-2023.webflow.c47b99761.css/g {} \;
$ find . -exec sed -i s/webflow.acaffe362.js.gz/webflow.acaffe362.js/g {} \;
```

Post-Conference Edits
---------------------
Changing the site to reflect the fact that the conference ended was
done after all of the above, rather than before. It involved the
following steps:

First, the pages as downloaded are stripped of whitespace, and are
impossible to work with. To start working, we used the
[tidy](https://www.html-tidy.org/) utility. We ran the command as

```console
$ tidy -m -i public/heb/home.html public/index.html
```

Then, we edited these files, to remove the top ticket links
and edit the "about" text, in both.

Next, we removed the "tickets" and "become a speaker" links from the
Hebrew footer.  This was done by:

- Editing public/heb/home.html manually to remove the links
- Creating a diff file:
  ```console
  $ git diff > footer-removal-heb.patch
  ```
- Using `tidy` as above to prepare the other files for editing
- Applying the diff file to the other files:
  ```console
  $ cd public
  $ patch -i ../footer-removal-heb.patch heb/speakers.html
  $ patch -i ../footer-removal-heb.patch heb/code-of-conduct.html
  ```
  (this almost worked, we still had to fix a little manually)

Next, we removed the "tickets" and "become a speaker" links in the
main English pages' footers. Generally, we followed the same process
as for Hebrew, though it went less smoothly. But this only took care of
the main pages -- inside pages (blog posts, and specific event and speaker
pages) were still not handled.

Then it was time to remove the "tickets" links in the inner pages. We
ran tidy over all of them, and repeated the same process as above --
pick a victim file, edit it, create a diff file, apply that diff to
all other files. Inner pages were different because the link in them
was different, so there was a different diff (compared to main pages).

The "become a speaker" links on the inner pages, however, were harder
to handle.  Many pages had the link with title "Become a Speaker";
some of them had it lead to the cfp (although it was closed already),
others had them point at the same page -- but explicitly naming it,
not using `href="#"` or some such relative point-to-self.
    
This, and the fact that after tidy the relevant elements were broken
across lines, made it difficult to remove these elements en masse.
    
What we did instead, is remove the link text:

```console
$ grep -l -r 'Become a Speaker' | xargs sed -i 's/Become a Speaker//'
```
and then add CSS rules that set the empty footer-link elements'
`display` to `none`, to make sure they do not affect the page layout.

The last step so far was an edit to change the contents of the PyPods
page.
